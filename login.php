<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css">
        <title>Document</title>
        <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    </head>
    <style>
    body {
        display: flex;
        justify-content: center;
    }

    .wrapper {
        border: 1px solid #41719c;
        padding-top: 40px;
        padding-right: 100px;
        padding-bottom: 15px;
        padding-left: 25px;
        display: inline-block;
    }

    .required:after {
        content:"*";
        color: red;
    }

    label {
        background-color: #70AD47;
        color: white;
        width: 80px;
        padding-left: 10px;
        padding-top: 10px;
        padding-bottom: 10px;
        padding-right: 10px;
        border: 1px solid #41719c;
        margin-bottom: 10px;
        display: inline-block;
    }

    input {
        width: 300px;
        line-height: 37px;
        margin-left: 10px;
        border: 1px solid #41719c;
    }

    select{
        width: 150px;
        height: 40px;
        line-height: 50px;
        margin-left: 10px;
        border: 1px solid #41719c;
    }

    input[type=text1]{
        width: 144px;
        line-height: 37px;
        margin-left: 10px;
        border: 1px solid #41719c;
    }

    input[type=radio]{
        width: auto;
        line-height: 37px;
        margin-left: 10px;
        border: 1px solid #70AD47;
    }

    input[type = submit] {
        width: auto;
        background-color: #70AD47;
        color: white;
        margin-left: 190px;
        border: 1px solid #41719c;
        padding: 5px 25px;
        border-radius: 7px;
        margin-top: 21px;
    }

    </style>
    <body>
        <div class = "wrapper">
        <form method="post" action="login.php">
        <?php

        function checkDateFormat($string) {
            $date_splitted = explode("/", $string);
            $date = "";
            if (count($date_splitted) == 3){
                $date = $date_splitted[1]."/".$date_splitted[0]."/".$date_splitted[2];
            } else {
                return 0;
            }
            if (strtotime($date)) {
                return 1;
            }
            return 0;
        }
        $data = array();

        if (!empty($_POST['contact_action'])){
            $data['name'] = isset($_POST['name']) ? $_POST['name'] : '';
            $data['gender'] = isset($_POST['gender']) ? $_POST['gender'] : '';
            $data['khoa'] = isset($_POST['khoa']) ? $_POST['khoa'] : '';
            $data['birthday'] = isset($_POST['birthday']) ? $_POST['birthday'] : '';
            $is_correct_date_format = checkDateFormat($data["birthday"]);

            if (empty($data["name"])) {
                echo '<p>
                <font face="Arial" style="text-align: center;  width: 100px;  color: red; margin-left: 20px;">
                Hãy nhập tên.</font></p> ';
            }

            if (empty($data["gender"])) {
                echo '<p>
                <font face="Arial" style="text-align: center;  width: 100px;  color: red; margin-left: 20px;">
                Hãy chọn giới tính.</font></p> ';
            }
        
            if (empty($data["khoa"])) {
                echo '<p>
                <font face="Arial" style="text-align: center;  width: 100px;  color: red; margin-left: 20px;">
                Hãy chọn phân khoa.</font></p> ';
            } 

            if (empty($data["birthday"])) {
                echo '<p>
                <font face="Arial" style="text-align: center;  width: 100px;  color: red; margin-left: 20px;">
                Hãy nhập ngày sinh.</font></p> ';
            } 
            if ($is_correct_date_format==0) {
                echo '<p>
                <font face="Arial" style="text-align: center;  width: 100px;  color: red; margin-left: 20px;">
                Hãy nhập ngày sinh đúng định dạng.</font></p> ';
            } 
        }
        ?>
            <div class = "name">
                <label class = "required">Họ và tên</label>
                <input type="text" name = "name">
            </div>

            <div class = "Gender">
                <label class = "required">Giới tính</label>
                <?php
                $gender = array("Nam", "Nữ");
                for ($x = 0; $x < count($gender); $x++){
                    echo "<input type =\"radio\" name = \"gender\"> $gender[$x]";
                }
                ?>
            </div>

            <div class = "khoa">
                <label class = "required">Phân khoa</label>
                <select name="khoa">
                    <?php
                    $khoa = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                    foreach($khoa as $x => $val){
                        echo "<option value=\"$x\">$val</option>";
                    }
                    ?>
                </select>
            </div>

            <div class = "birthday">
                <label class = "required">Ngày sinh</label>
                <input type="text1" name="birthday" placeholder="dd/mm/yyyy">
            </div>

            <div class = "Address">
                <label >Địa chỉ</label>
                <input type="text" name = "address">
            </div>
            <input type="submit" name="contact_action" value="Đăng ký"/></td>
        </form>
        </div>
    </body>
</html>
